/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import uesocc.edu.sv.ingenieria.prn335.entidades.Modelo;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.TipoVehiculo;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class TipoVehiculoJpaController implements Serializable {

    public TipoVehiculoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoVehiculo tipoVehiculo) throws RollbackFailureException, Exception {
        if (tipoVehiculo.getModeloList() == null) {
            tipoVehiculo.setModeloList(new ArrayList<Modelo>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Modelo> attachedModeloList = new ArrayList<Modelo>();
            for (Modelo modeloListModeloToAttach : tipoVehiculo.getModeloList()) {
                modeloListModeloToAttach = em.getReference(modeloListModeloToAttach.getClass(), modeloListModeloToAttach.getIdModelo());
                attachedModeloList.add(modeloListModeloToAttach);
            }
            tipoVehiculo.setModeloList(attachedModeloList);
            em.persist(tipoVehiculo);
            for (Modelo modeloListModelo : tipoVehiculo.getModeloList()) {
                TipoVehiculo oldIdTipoVehiculoOfModeloListModelo = modeloListModelo.getIdTipoVehiculo();
                modeloListModelo.setIdTipoVehiculo(tipoVehiculo);
                modeloListModelo = em.merge(modeloListModelo);
                if (oldIdTipoVehiculoOfModeloListModelo != null) {
                    oldIdTipoVehiculoOfModeloListModelo.getModeloList().remove(modeloListModelo);
                    oldIdTipoVehiculoOfModeloListModelo = em.merge(oldIdTipoVehiculoOfModeloListModelo);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoVehiculo tipoVehiculo) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoVehiculo persistentTipoVehiculo = em.find(TipoVehiculo.class, tipoVehiculo.getIdTipoVehiculo());
            List<Modelo> modeloListOld = persistentTipoVehiculo.getModeloList();
            List<Modelo> modeloListNew = tipoVehiculo.getModeloList();
            List<Modelo> attachedModeloListNew = new ArrayList<Modelo>();
            for (Modelo modeloListNewModeloToAttach : modeloListNew) {
                modeloListNewModeloToAttach = em.getReference(modeloListNewModeloToAttach.getClass(), modeloListNewModeloToAttach.getIdModelo());
                attachedModeloListNew.add(modeloListNewModeloToAttach);
            }
            modeloListNew = attachedModeloListNew;
            tipoVehiculo.setModeloList(modeloListNew);
            tipoVehiculo = em.merge(tipoVehiculo);
            for (Modelo modeloListOldModelo : modeloListOld) {
                if (!modeloListNew.contains(modeloListOldModelo)) {
                    modeloListOldModelo.setIdTipoVehiculo(null);
                    modeloListOldModelo = em.merge(modeloListOldModelo);
                }
            }
            for (Modelo modeloListNewModelo : modeloListNew) {
                if (!modeloListOld.contains(modeloListNewModelo)) {
                    TipoVehiculo oldIdTipoVehiculoOfModeloListNewModelo = modeloListNewModelo.getIdTipoVehiculo();
                    modeloListNewModelo.setIdTipoVehiculo(tipoVehiculo);
                    modeloListNewModelo = em.merge(modeloListNewModelo);
                    if (oldIdTipoVehiculoOfModeloListNewModelo != null && !oldIdTipoVehiculoOfModeloListNewModelo.equals(tipoVehiculo)) {
                        oldIdTipoVehiculoOfModeloListNewModelo.getModeloList().remove(modeloListNewModelo);
                        oldIdTipoVehiculoOfModeloListNewModelo = em.merge(oldIdTipoVehiculoOfModeloListNewModelo);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipoVehiculo.getIdTipoVehiculo();
                if (findTipoVehiculo(id) == null) {
                    throw new NonexistentEntityException("The tipoVehiculo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoVehiculo tipoVehiculo;
            try {
                tipoVehiculo = em.getReference(TipoVehiculo.class, id);
                tipoVehiculo.getIdTipoVehiculo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoVehiculo with id " + id + " no longer exists.", enfe);
            }
            List<Modelo> modeloList = tipoVehiculo.getModeloList();
            for (Modelo modeloListModelo : modeloList) {
                modeloListModelo.setIdTipoVehiculo(null);
                modeloListModelo = em.merge(modeloListModelo);
            }
            em.remove(tipoVehiculo);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoVehiculo> findTipoVehiculoEntities() {
        return findTipoVehiculoEntities(true, -1, -1);
    }

    public List<TipoVehiculo> findTipoVehiculoEntities(int maxResults, int firstResult) {
        return findTipoVehiculoEntities(false, maxResults, firstResult);
    }

    private List<TipoVehiculo> findTipoVehiculoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoVehiculo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoVehiculo findTipoVehiculo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoVehiculo.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoVehiculoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoVehiculo> rt = cq.from(TipoVehiculo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
