/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import uesocc.edu.sv.ingenieria.prn335.entidades.Reserva;
import uesocc.edu.sv.ingenieria.prn335.entidades.Recorrido;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.Viaje;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.IllegalOrphanException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.PreexistingEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class ViajeJpaController implements Serializable {

    public ViajeJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Viaje viaje) throws IllegalOrphanException, PreexistingEntityException, RollbackFailureException, Exception {
        if (viaje.getRecorridoList() == null) {
            viaje.setRecorridoList(new ArrayList<Recorrido>());
        }
        List<String> illegalOrphanMessages = null;
        Reserva reservaOrphanCheck = viaje.getReserva();
        if (reservaOrphanCheck != null) {
            Viaje oldViajeOfReserva = reservaOrphanCheck.getViaje();
            if (oldViajeOfReserva != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("The Reserva " + reservaOrphanCheck + " already has an item of type Viaje whose reserva column cannot be null. Please make another selection for the reserva field.");
            }
        }
        if (illegalOrphanMessages != null) {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Reserva reserva = viaje.getReserva();
            if (reserva != null) {
                reserva = em.getReference(reserva.getClass(), reserva.getIdReserva());
                viaje.setReserva(reserva);
            }
            List<Recorrido> attachedRecorridoList = new ArrayList<Recorrido>();
            for (Recorrido recorridoListRecorridoToAttach : viaje.getRecorridoList()) {
                recorridoListRecorridoToAttach = em.getReference(recorridoListRecorridoToAttach.getClass(), recorridoListRecorridoToAttach.getIdRecorrido());
                attachedRecorridoList.add(recorridoListRecorridoToAttach);
            }
            viaje.setRecorridoList(attachedRecorridoList);
            em.persist(viaje);
            if (reserva != null) {
                reserva.setViaje(viaje);
                reserva = em.merge(reserva);
            }
            for (Recorrido recorridoListRecorrido : viaje.getRecorridoList()) {
                Viaje oldIdReservaOfRecorridoListRecorrido = recorridoListRecorrido.getIdReserva();
                recorridoListRecorrido.setIdReserva(viaje);
                recorridoListRecorrido = em.merge(recorridoListRecorrido);
                if (oldIdReservaOfRecorridoListRecorrido != null) {
                    oldIdReservaOfRecorridoListRecorrido.getRecorridoList().remove(recorridoListRecorrido);
                    oldIdReservaOfRecorridoListRecorrido = em.merge(oldIdReservaOfRecorridoListRecorrido);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findViaje(viaje.getIdReserva()) != null) {
                throw new PreexistingEntityException("Viaje " + viaje + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Viaje viaje) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Viaje persistentViaje = em.find(Viaje.class, viaje.getIdReserva());
            Reserva reservaOld = persistentViaje.getReserva();
            Reserva reservaNew = viaje.getReserva();
            List<Recorrido> recorridoListOld = persistentViaje.getRecorridoList();
            List<Recorrido> recorridoListNew = viaje.getRecorridoList();
            List<String> illegalOrphanMessages = null;
            if (reservaNew != null && !reservaNew.equals(reservaOld)) {
                Viaje oldViajeOfReserva = reservaNew.getViaje();
                if (oldViajeOfReserva != null) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("The Reserva " + reservaNew + " already has an item of type Viaje whose reserva column cannot be null. Please make another selection for the reserva field.");
                }
            }
            for (Recorrido recorridoListOldRecorrido : recorridoListOld) {
                if (!recorridoListNew.contains(recorridoListOldRecorrido)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Recorrido " + recorridoListOldRecorrido + " since its idReserva field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (reservaNew != null) {
                reservaNew = em.getReference(reservaNew.getClass(), reservaNew.getIdReserva());
                viaje.setReserva(reservaNew);
            }
            List<Recorrido> attachedRecorridoListNew = new ArrayList<Recorrido>();
            for (Recorrido recorridoListNewRecorridoToAttach : recorridoListNew) {
                recorridoListNewRecorridoToAttach = em.getReference(recorridoListNewRecorridoToAttach.getClass(), recorridoListNewRecorridoToAttach.getIdRecorrido());
                attachedRecorridoListNew.add(recorridoListNewRecorridoToAttach);
            }
            recorridoListNew = attachedRecorridoListNew;
            viaje.setRecorridoList(recorridoListNew);
            viaje = em.merge(viaje);
            if (reservaOld != null && !reservaOld.equals(reservaNew)) {
                reservaOld.setViaje(null);
                reservaOld = em.merge(reservaOld);
            }
            if (reservaNew != null && !reservaNew.equals(reservaOld)) {
                reservaNew.setViaje(viaje);
                reservaNew = em.merge(reservaNew);
            }
            for (Recorrido recorridoListNewRecorrido : recorridoListNew) {
                if (!recorridoListOld.contains(recorridoListNewRecorrido)) {
                    Viaje oldIdReservaOfRecorridoListNewRecorrido = recorridoListNewRecorrido.getIdReserva();
                    recorridoListNewRecorrido.setIdReserva(viaje);
                    recorridoListNewRecorrido = em.merge(recorridoListNewRecorrido);
                    if (oldIdReservaOfRecorridoListNewRecorrido != null && !oldIdReservaOfRecorridoListNewRecorrido.equals(viaje)) {
                        oldIdReservaOfRecorridoListNewRecorrido.getRecorridoList().remove(recorridoListNewRecorrido);
                        oldIdReservaOfRecorridoListNewRecorrido = em.merge(oldIdReservaOfRecorridoListNewRecorrido);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = viaje.getIdReserva();
                if (findViaje(id) == null) {
                    throw new NonexistentEntityException("The viaje with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Viaje viaje;
            try {
                viaje = em.getReference(Viaje.class, id);
                viaje.getIdReserva();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The viaje with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Recorrido> recorridoListOrphanCheck = viaje.getRecorridoList();
            for (Recorrido recorridoListOrphanCheckRecorrido : recorridoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Viaje (" + viaje + ") cannot be destroyed since the Recorrido " + recorridoListOrphanCheckRecorrido + " in its recorridoList field has a non-nullable idReserva field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Reserva reserva = viaje.getReserva();
            if (reserva != null) {
                reserva.setViaje(null);
                reserva = em.merge(reserva);
            }
            em.remove(viaje);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Viaje> findViajeEntities() {
        return findViajeEntities(true, -1, -1);
    }

    public List<Viaje> findViajeEntities(int maxResults, int firstResult) {
        return findViajeEntities(false, maxResults, firstResult);
    }

    private List<Viaje> findViajeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Viaje.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Viaje findViaje(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Viaje.class, id);
        } finally {
            em.close();
        }
    }

    public int getViajeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Viaje> rt = cq.from(Viaje.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
