/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import uesocc.edu.sv.ingenieria.prn335.entidades.EstadoVehiculo;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.TipoEstadoVehiculo;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.IllegalOrphanException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class TipoEstadoVehiculoJpaController implements Serializable {

    public TipoEstadoVehiculoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoEstadoVehiculo tipoEstadoVehiculo) throws RollbackFailureException, Exception {
        if (tipoEstadoVehiculo.getEstadoVehiculoList() == null) {
            tipoEstadoVehiculo.setEstadoVehiculoList(new ArrayList<EstadoVehiculo>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<EstadoVehiculo> attachedEstadoVehiculoList = new ArrayList<EstadoVehiculo>();
            for (EstadoVehiculo estadoVehiculoListEstadoVehiculoToAttach : tipoEstadoVehiculo.getEstadoVehiculoList()) {
                estadoVehiculoListEstadoVehiculoToAttach = em.getReference(estadoVehiculoListEstadoVehiculoToAttach.getClass(), estadoVehiculoListEstadoVehiculoToAttach.getIdEstadoVehiculo());
                attachedEstadoVehiculoList.add(estadoVehiculoListEstadoVehiculoToAttach);
            }
            tipoEstadoVehiculo.setEstadoVehiculoList(attachedEstadoVehiculoList);
            em.persist(tipoEstadoVehiculo);
            for (EstadoVehiculo estadoVehiculoListEstadoVehiculo : tipoEstadoVehiculo.getEstadoVehiculoList()) {
                TipoEstadoVehiculo oldIdTipoEstadoVehiculoOfEstadoVehiculoListEstadoVehiculo = estadoVehiculoListEstadoVehiculo.getIdTipoEstadoVehiculo();
                estadoVehiculoListEstadoVehiculo.setIdTipoEstadoVehiculo(tipoEstadoVehiculo);
                estadoVehiculoListEstadoVehiculo = em.merge(estadoVehiculoListEstadoVehiculo);
                if (oldIdTipoEstadoVehiculoOfEstadoVehiculoListEstadoVehiculo != null) {
                    oldIdTipoEstadoVehiculoOfEstadoVehiculoListEstadoVehiculo.getEstadoVehiculoList().remove(estadoVehiculoListEstadoVehiculo);
                    oldIdTipoEstadoVehiculoOfEstadoVehiculoListEstadoVehiculo = em.merge(oldIdTipoEstadoVehiculoOfEstadoVehiculoListEstadoVehiculo);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoEstadoVehiculo tipoEstadoVehiculo) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoEstadoVehiculo persistentTipoEstadoVehiculo = em.find(TipoEstadoVehiculo.class, tipoEstadoVehiculo.getIdTipoEstadoVehiculo());
            List<EstadoVehiculo> estadoVehiculoListOld = persistentTipoEstadoVehiculo.getEstadoVehiculoList();
            List<EstadoVehiculo> estadoVehiculoListNew = tipoEstadoVehiculo.getEstadoVehiculoList();
            List<String> illegalOrphanMessages = null;
            for (EstadoVehiculo estadoVehiculoListOldEstadoVehiculo : estadoVehiculoListOld) {
                if (!estadoVehiculoListNew.contains(estadoVehiculoListOldEstadoVehiculo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain EstadoVehiculo " + estadoVehiculoListOldEstadoVehiculo + " since its idTipoEstadoVehiculo field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<EstadoVehiculo> attachedEstadoVehiculoListNew = new ArrayList<EstadoVehiculo>();
            for (EstadoVehiculo estadoVehiculoListNewEstadoVehiculoToAttach : estadoVehiculoListNew) {
                estadoVehiculoListNewEstadoVehiculoToAttach = em.getReference(estadoVehiculoListNewEstadoVehiculoToAttach.getClass(), estadoVehiculoListNewEstadoVehiculoToAttach.getIdEstadoVehiculo());
                attachedEstadoVehiculoListNew.add(estadoVehiculoListNewEstadoVehiculoToAttach);
            }
            estadoVehiculoListNew = attachedEstadoVehiculoListNew;
            tipoEstadoVehiculo.setEstadoVehiculoList(estadoVehiculoListNew);
            tipoEstadoVehiculo = em.merge(tipoEstadoVehiculo);
            for (EstadoVehiculo estadoVehiculoListNewEstadoVehiculo : estadoVehiculoListNew) {
                if (!estadoVehiculoListOld.contains(estadoVehiculoListNewEstadoVehiculo)) {
                    TipoEstadoVehiculo oldIdTipoEstadoVehiculoOfEstadoVehiculoListNewEstadoVehiculo = estadoVehiculoListNewEstadoVehiculo.getIdTipoEstadoVehiculo();
                    estadoVehiculoListNewEstadoVehiculo.setIdTipoEstadoVehiculo(tipoEstadoVehiculo);
                    estadoVehiculoListNewEstadoVehiculo = em.merge(estadoVehiculoListNewEstadoVehiculo);
                    if (oldIdTipoEstadoVehiculoOfEstadoVehiculoListNewEstadoVehiculo != null && !oldIdTipoEstadoVehiculoOfEstadoVehiculoListNewEstadoVehiculo.equals(tipoEstadoVehiculo)) {
                        oldIdTipoEstadoVehiculoOfEstadoVehiculoListNewEstadoVehiculo.getEstadoVehiculoList().remove(estadoVehiculoListNewEstadoVehiculo);
                        oldIdTipoEstadoVehiculoOfEstadoVehiculoListNewEstadoVehiculo = em.merge(oldIdTipoEstadoVehiculoOfEstadoVehiculoListNewEstadoVehiculo);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipoEstadoVehiculo.getIdTipoEstadoVehiculo();
                if (findTipoEstadoVehiculo(id) == null) {
                    throw new NonexistentEntityException("The tipoEstadoVehiculo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoEstadoVehiculo tipoEstadoVehiculo;
            try {
                tipoEstadoVehiculo = em.getReference(TipoEstadoVehiculo.class, id);
                tipoEstadoVehiculo.getIdTipoEstadoVehiculo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoEstadoVehiculo with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<EstadoVehiculo> estadoVehiculoListOrphanCheck = tipoEstadoVehiculo.getEstadoVehiculoList();
            for (EstadoVehiculo estadoVehiculoListOrphanCheckEstadoVehiculo : estadoVehiculoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TipoEstadoVehiculo (" + tipoEstadoVehiculo + ") cannot be destroyed since the EstadoVehiculo " + estadoVehiculoListOrphanCheckEstadoVehiculo + " in its estadoVehiculoList field has a non-nullable idTipoEstadoVehiculo field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipoEstadoVehiculo);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoEstadoVehiculo> findTipoEstadoVehiculoEntities() {
        return findTipoEstadoVehiculoEntities(true, -1, -1);
    }

    public List<TipoEstadoVehiculo> findTipoEstadoVehiculoEntities(int maxResults, int firstResult) {
        return findTipoEstadoVehiculoEntities(false, maxResults, firstResult);
    }

    private List<TipoEstadoVehiculo> findTipoEstadoVehiculoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoEstadoVehiculo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoEstadoVehiculo findTipoEstadoVehiculo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoEstadoVehiculo.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoEstadoVehiculoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoEstadoVehiculo> rt = cq.from(TipoEstadoVehiculo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
