/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import uesocc.edu.sv.ingenieria.prn335.entidades.Modelo;
import uesocc.edu.sv.ingenieria.prn335.entidades.EstadoVehiculo;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.Reserva;
import uesocc.edu.sv.ingenieria.prn335.entidades.Vehiculo;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.IllegalOrphanException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class VehiculoJpaController implements Serializable {

    public VehiculoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Vehiculo vehiculo) throws RollbackFailureException, Exception {
        if (vehiculo.getEstadoVehiculoList() == null) {
            vehiculo.setEstadoVehiculoList(new ArrayList<EstadoVehiculo>());
        }
        if (vehiculo.getReservaList() == null) {
            vehiculo.setReservaList(new ArrayList<Reserva>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Modelo idModelo = vehiculo.getIdModelo();
            if (idModelo != null) {
                idModelo = em.getReference(idModelo.getClass(), idModelo.getIdModelo());
                vehiculo.setIdModelo(idModelo);
            }
            List<EstadoVehiculo> attachedEstadoVehiculoList = new ArrayList<EstadoVehiculo>();
            for (EstadoVehiculo estadoVehiculoListEstadoVehiculoToAttach : vehiculo.getEstadoVehiculoList()) {
                estadoVehiculoListEstadoVehiculoToAttach = em.getReference(estadoVehiculoListEstadoVehiculoToAttach.getClass(), estadoVehiculoListEstadoVehiculoToAttach.getIdEstadoVehiculo());
                attachedEstadoVehiculoList.add(estadoVehiculoListEstadoVehiculoToAttach);
            }
            vehiculo.setEstadoVehiculoList(attachedEstadoVehiculoList);
            List<Reserva> attachedReservaList = new ArrayList<Reserva>();
            for (Reserva reservaListReservaToAttach : vehiculo.getReservaList()) {
                reservaListReservaToAttach = em.getReference(reservaListReservaToAttach.getClass(), reservaListReservaToAttach.getIdReserva());
                attachedReservaList.add(reservaListReservaToAttach);
            }
            vehiculo.setReservaList(attachedReservaList);
            em.persist(vehiculo);
            if (idModelo != null) {
                idModelo.getVehiculoList().add(vehiculo);
                idModelo = em.merge(idModelo);
            }
            for (EstadoVehiculo estadoVehiculoListEstadoVehiculo : vehiculo.getEstadoVehiculoList()) {
                Vehiculo oldIdVehiculoOfEstadoVehiculoListEstadoVehiculo = estadoVehiculoListEstadoVehiculo.getIdVehiculo();
                estadoVehiculoListEstadoVehiculo.setIdVehiculo(vehiculo);
                estadoVehiculoListEstadoVehiculo = em.merge(estadoVehiculoListEstadoVehiculo);
                if (oldIdVehiculoOfEstadoVehiculoListEstadoVehiculo != null) {
                    oldIdVehiculoOfEstadoVehiculoListEstadoVehiculo.getEstadoVehiculoList().remove(estadoVehiculoListEstadoVehiculo);
                    oldIdVehiculoOfEstadoVehiculoListEstadoVehiculo = em.merge(oldIdVehiculoOfEstadoVehiculoListEstadoVehiculo);
                }
            }
            for (Reserva reservaListReserva : vehiculo.getReservaList()) {
                Vehiculo oldIdVehiculoOfReservaListReserva = reservaListReserva.getIdVehiculo();
                reservaListReserva.setIdVehiculo(vehiculo);
                reservaListReserva = em.merge(reservaListReserva);
                if (oldIdVehiculoOfReservaListReserva != null) {
                    oldIdVehiculoOfReservaListReserva.getReservaList().remove(reservaListReserva);
                    oldIdVehiculoOfReservaListReserva = em.merge(oldIdVehiculoOfReservaListReserva);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Vehiculo vehiculo) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Vehiculo persistentVehiculo = em.find(Vehiculo.class, vehiculo.getIdVehiculo());
            Modelo idModeloOld = persistentVehiculo.getIdModelo();
            Modelo idModeloNew = vehiculo.getIdModelo();
            List<EstadoVehiculo> estadoVehiculoListOld = persistentVehiculo.getEstadoVehiculoList();
            List<EstadoVehiculo> estadoVehiculoListNew = vehiculo.getEstadoVehiculoList();
            List<Reserva> reservaListOld = persistentVehiculo.getReservaList();
            List<Reserva> reservaListNew = vehiculo.getReservaList();
            List<String> illegalOrphanMessages = null;
            for (EstadoVehiculo estadoVehiculoListOldEstadoVehiculo : estadoVehiculoListOld) {
                if (!estadoVehiculoListNew.contains(estadoVehiculoListOldEstadoVehiculo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain EstadoVehiculo " + estadoVehiculoListOldEstadoVehiculo + " since its idVehiculo field is not nullable.");
                }
            }
            for (Reserva reservaListOldReserva : reservaListOld) {
                if (!reservaListNew.contains(reservaListOldReserva)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Reserva " + reservaListOldReserva + " since its idVehiculo field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idModeloNew != null) {
                idModeloNew = em.getReference(idModeloNew.getClass(), idModeloNew.getIdModelo());
                vehiculo.setIdModelo(idModeloNew);
            }
            List<EstadoVehiculo> attachedEstadoVehiculoListNew = new ArrayList<EstadoVehiculo>();
            for (EstadoVehiculo estadoVehiculoListNewEstadoVehiculoToAttach : estadoVehiculoListNew) {
                estadoVehiculoListNewEstadoVehiculoToAttach = em.getReference(estadoVehiculoListNewEstadoVehiculoToAttach.getClass(), estadoVehiculoListNewEstadoVehiculoToAttach.getIdEstadoVehiculo());
                attachedEstadoVehiculoListNew.add(estadoVehiculoListNewEstadoVehiculoToAttach);
            }
            estadoVehiculoListNew = attachedEstadoVehiculoListNew;
            vehiculo.setEstadoVehiculoList(estadoVehiculoListNew);
            List<Reserva> attachedReservaListNew = new ArrayList<Reserva>();
            for (Reserva reservaListNewReservaToAttach : reservaListNew) {
                reservaListNewReservaToAttach = em.getReference(reservaListNewReservaToAttach.getClass(), reservaListNewReservaToAttach.getIdReserva());
                attachedReservaListNew.add(reservaListNewReservaToAttach);
            }
            reservaListNew = attachedReservaListNew;
            vehiculo.setReservaList(reservaListNew);
            vehiculo = em.merge(vehiculo);
            if (idModeloOld != null && !idModeloOld.equals(idModeloNew)) {
                idModeloOld.getVehiculoList().remove(vehiculo);
                idModeloOld = em.merge(idModeloOld);
            }
            if (idModeloNew != null && !idModeloNew.equals(idModeloOld)) {
                idModeloNew.getVehiculoList().add(vehiculo);
                idModeloNew = em.merge(idModeloNew);
            }
            for (EstadoVehiculo estadoVehiculoListNewEstadoVehiculo : estadoVehiculoListNew) {
                if (!estadoVehiculoListOld.contains(estadoVehiculoListNewEstadoVehiculo)) {
                    Vehiculo oldIdVehiculoOfEstadoVehiculoListNewEstadoVehiculo = estadoVehiculoListNewEstadoVehiculo.getIdVehiculo();
                    estadoVehiculoListNewEstadoVehiculo.setIdVehiculo(vehiculo);
                    estadoVehiculoListNewEstadoVehiculo = em.merge(estadoVehiculoListNewEstadoVehiculo);
                    if (oldIdVehiculoOfEstadoVehiculoListNewEstadoVehiculo != null && !oldIdVehiculoOfEstadoVehiculoListNewEstadoVehiculo.equals(vehiculo)) {
                        oldIdVehiculoOfEstadoVehiculoListNewEstadoVehiculo.getEstadoVehiculoList().remove(estadoVehiculoListNewEstadoVehiculo);
                        oldIdVehiculoOfEstadoVehiculoListNewEstadoVehiculo = em.merge(oldIdVehiculoOfEstadoVehiculoListNewEstadoVehiculo);
                    }
                }
            }
            for (Reserva reservaListNewReserva : reservaListNew) {
                if (!reservaListOld.contains(reservaListNewReserva)) {
                    Vehiculo oldIdVehiculoOfReservaListNewReserva = reservaListNewReserva.getIdVehiculo();
                    reservaListNewReserva.setIdVehiculo(vehiculo);
                    reservaListNewReserva = em.merge(reservaListNewReserva);
                    if (oldIdVehiculoOfReservaListNewReserva != null && !oldIdVehiculoOfReservaListNewReserva.equals(vehiculo)) {
                        oldIdVehiculoOfReservaListNewReserva.getReservaList().remove(reservaListNewReserva);
                        oldIdVehiculoOfReservaListNewReserva = em.merge(oldIdVehiculoOfReservaListNewReserva);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = vehiculo.getIdVehiculo();
                if (findVehiculo(id) == null) {
                    throw new NonexistentEntityException("The vehiculo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Vehiculo vehiculo;
            try {
                vehiculo = em.getReference(Vehiculo.class, id);
                vehiculo.getIdVehiculo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The vehiculo with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<EstadoVehiculo> estadoVehiculoListOrphanCheck = vehiculo.getEstadoVehiculoList();
            for (EstadoVehiculo estadoVehiculoListOrphanCheckEstadoVehiculo : estadoVehiculoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Vehiculo (" + vehiculo + ") cannot be destroyed since the EstadoVehiculo " + estadoVehiculoListOrphanCheckEstadoVehiculo + " in its estadoVehiculoList field has a non-nullable idVehiculo field.");
            }
            List<Reserva> reservaListOrphanCheck = vehiculo.getReservaList();
            for (Reserva reservaListOrphanCheckReserva : reservaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Vehiculo (" + vehiculo + ") cannot be destroyed since the Reserva " + reservaListOrphanCheckReserva + " in its reservaList field has a non-nullable idVehiculo field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Modelo idModelo = vehiculo.getIdModelo();
            if (idModelo != null) {
                idModelo.getVehiculoList().remove(vehiculo);
                idModelo = em.merge(idModelo);
            }
            em.remove(vehiculo);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Vehiculo> findVehiculoEntities() {
        return findVehiculoEntities(true, -1, -1);
    }

    public List<Vehiculo> findVehiculoEntities(int maxResults, int firstResult) {
        return findVehiculoEntities(false, maxResults, firstResult);
    }

    private List<Vehiculo> findVehiculoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Vehiculo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Vehiculo findVehiculo(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Vehiculo.class, id);
        } finally {
            em.close();
        }
    }

    public int getVehiculoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Vehiculo> rt = cq.from(Vehiculo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
