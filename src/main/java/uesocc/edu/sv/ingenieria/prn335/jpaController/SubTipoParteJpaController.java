/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import uesocc.edu.sv.ingenieria.prn335.entidades.TipoParte;
import uesocc.edu.sv.ingenieria.prn335.entidades.Parte;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.SubTipoParte;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.IllegalOrphanException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class SubTipoParteJpaController implements Serializable {

    public SubTipoParteJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SubTipoParte subTipoParte) throws RollbackFailureException, Exception {
        if (subTipoParte.getParteList() == null) {
            subTipoParte.setParteList(new ArrayList<Parte>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoParte idTipoParte = subTipoParte.getIdTipoParte();
            if (idTipoParte != null) {
                idTipoParte = em.getReference(idTipoParte.getClass(), idTipoParte.getIdTipoParte());
                subTipoParte.setIdTipoParte(idTipoParte);
            }
            List<Parte> attachedParteList = new ArrayList<Parte>();
            for (Parte parteListParteToAttach : subTipoParte.getParteList()) {
                parteListParteToAttach = em.getReference(parteListParteToAttach.getClass(), parteListParteToAttach.getIdParte());
                attachedParteList.add(parteListParteToAttach);
            }
            subTipoParte.setParteList(attachedParteList);
            em.persist(subTipoParte);
            if (idTipoParte != null) {
                idTipoParte.getSubTipoParteList().add(subTipoParte);
                idTipoParte = em.merge(idTipoParte);
            }
            for (Parte parteListParte : subTipoParte.getParteList()) {
                SubTipoParte oldIdSubTipoParteOfParteListParte = parteListParte.getIdSubTipoParte();
                parteListParte.setIdSubTipoParte(subTipoParte);
                parteListParte = em.merge(parteListParte);
                if (oldIdSubTipoParteOfParteListParte != null) {
                    oldIdSubTipoParteOfParteListParte.getParteList().remove(parteListParte);
                    oldIdSubTipoParteOfParteListParte = em.merge(oldIdSubTipoParteOfParteListParte);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SubTipoParte subTipoParte) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            SubTipoParte persistentSubTipoParte = em.find(SubTipoParte.class, subTipoParte.getIdSubTipoParte());
            TipoParte idTipoParteOld = persistentSubTipoParte.getIdTipoParte();
            TipoParte idTipoParteNew = subTipoParte.getIdTipoParte();
            List<Parte> parteListOld = persistentSubTipoParte.getParteList();
            List<Parte> parteListNew = subTipoParte.getParteList();
            List<String> illegalOrphanMessages = null;
            for (Parte parteListOldParte : parteListOld) {
                if (!parteListNew.contains(parteListOldParte)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Parte " + parteListOldParte + " since its idSubTipoParte field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idTipoParteNew != null) {
                idTipoParteNew = em.getReference(idTipoParteNew.getClass(), idTipoParteNew.getIdTipoParte());
                subTipoParte.setIdTipoParte(idTipoParteNew);
            }
            List<Parte> attachedParteListNew = new ArrayList<Parte>();
            for (Parte parteListNewParteToAttach : parteListNew) {
                parteListNewParteToAttach = em.getReference(parteListNewParteToAttach.getClass(), parteListNewParteToAttach.getIdParte());
                attachedParteListNew.add(parteListNewParteToAttach);
            }
            parteListNew = attachedParteListNew;
            subTipoParte.setParteList(parteListNew);
            subTipoParte = em.merge(subTipoParte);
            if (idTipoParteOld != null && !idTipoParteOld.equals(idTipoParteNew)) {
                idTipoParteOld.getSubTipoParteList().remove(subTipoParte);
                idTipoParteOld = em.merge(idTipoParteOld);
            }
            if (idTipoParteNew != null && !idTipoParteNew.equals(idTipoParteOld)) {
                idTipoParteNew.getSubTipoParteList().add(subTipoParte);
                idTipoParteNew = em.merge(idTipoParteNew);
            }
            for (Parte parteListNewParte : parteListNew) {
                if (!parteListOld.contains(parteListNewParte)) {
                    SubTipoParte oldIdSubTipoParteOfParteListNewParte = parteListNewParte.getIdSubTipoParte();
                    parteListNewParte.setIdSubTipoParte(subTipoParte);
                    parteListNewParte = em.merge(parteListNewParte);
                    if (oldIdSubTipoParteOfParteListNewParte != null && !oldIdSubTipoParteOfParteListNewParte.equals(subTipoParte)) {
                        oldIdSubTipoParteOfParteListNewParte.getParteList().remove(parteListNewParte);
                        oldIdSubTipoParteOfParteListNewParte = em.merge(oldIdSubTipoParteOfParteListNewParte);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = subTipoParte.getIdSubTipoParte();
                if (findSubTipoParte(id) == null) {
                    throw new NonexistentEntityException("The subTipoParte with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            SubTipoParte subTipoParte;
            try {
                subTipoParte = em.getReference(SubTipoParte.class, id);
                subTipoParte.getIdSubTipoParte();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The subTipoParte with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Parte> parteListOrphanCheck = subTipoParte.getParteList();
            for (Parte parteListOrphanCheckParte : parteListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This SubTipoParte (" + subTipoParte + ") cannot be destroyed since the Parte " + parteListOrphanCheckParte + " in its parteList field has a non-nullable idSubTipoParte field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            TipoParte idTipoParte = subTipoParte.getIdTipoParte();
            if (idTipoParte != null) {
                idTipoParte.getSubTipoParteList().remove(subTipoParte);
                idTipoParte = em.merge(idTipoParte);
            }
            em.remove(subTipoParte);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SubTipoParte> findSubTipoParteEntities() {
        return findSubTipoParteEntities(true, -1, -1);
    }

    public List<SubTipoParte> findSubTipoParteEntities(int maxResults, int firstResult) {
        return findSubTipoParteEntities(false, maxResults, firstResult);
    }

    private List<SubTipoParte> findSubTipoParteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SubTipoParte.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SubTipoParte findSubTipoParte(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SubTipoParte.class, id);
        } finally {
            em.close();
        }
    }

    public int getSubTipoParteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SubTipoParte> rt = cq.from(SubTipoParte.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
