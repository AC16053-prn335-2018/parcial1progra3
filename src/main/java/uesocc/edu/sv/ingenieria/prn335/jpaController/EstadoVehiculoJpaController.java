/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.EstadoVehiculo;
import uesocc.edu.sv.ingenieria.prn335.entidades.TipoEstadoVehiculo;
import uesocc.edu.sv.ingenieria.prn335.entidades.Vehiculo;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class EstadoVehiculoJpaController implements Serializable {

    public EstadoVehiculoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(EstadoVehiculo estadoVehiculo) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoEstadoVehiculo idTipoEstadoVehiculo = estadoVehiculo.getIdTipoEstadoVehiculo();
            if (idTipoEstadoVehiculo != null) {
                idTipoEstadoVehiculo = em.getReference(idTipoEstadoVehiculo.getClass(), idTipoEstadoVehiculo.getIdTipoEstadoVehiculo());
                estadoVehiculo.setIdTipoEstadoVehiculo(idTipoEstadoVehiculo);
            }
            Vehiculo idVehiculo = estadoVehiculo.getIdVehiculo();
            if (idVehiculo != null) {
                idVehiculo = em.getReference(idVehiculo.getClass(), idVehiculo.getIdVehiculo());
                estadoVehiculo.setIdVehiculo(idVehiculo);
            }
            em.persist(estadoVehiculo);
            if (idTipoEstadoVehiculo != null) {
                idTipoEstadoVehiculo.getEstadoVehiculoList().add(estadoVehiculo);
                idTipoEstadoVehiculo = em.merge(idTipoEstadoVehiculo);
            }
            if (idVehiculo != null) {
                idVehiculo.getEstadoVehiculoList().add(estadoVehiculo);
                idVehiculo = em.merge(idVehiculo);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(EstadoVehiculo estadoVehiculo) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            EstadoVehiculo persistentEstadoVehiculo = em.find(EstadoVehiculo.class, estadoVehiculo.getIdEstadoVehiculo());
            TipoEstadoVehiculo idTipoEstadoVehiculoOld = persistentEstadoVehiculo.getIdTipoEstadoVehiculo();
            TipoEstadoVehiculo idTipoEstadoVehiculoNew = estadoVehiculo.getIdTipoEstadoVehiculo();
            Vehiculo idVehiculoOld = persistentEstadoVehiculo.getIdVehiculo();
            Vehiculo idVehiculoNew = estadoVehiculo.getIdVehiculo();
            if (idTipoEstadoVehiculoNew != null) {
                idTipoEstadoVehiculoNew = em.getReference(idTipoEstadoVehiculoNew.getClass(), idTipoEstadoVehiculoNew.getIdTipoEstadoVehiculo());
                estadoVehiculo.setIdTipoEstadoVehiculo(idTipoEstadoVehiculoNew);
            }
            if (idVehiculoNew != null) {
                idVehiculoNew = em.getReference(idVehiculoNew.getClass(), idVehiculoNew.getIdVehiculo());
                estadoVehiculo.setIdVehiculo(idVehiculoNew);
            }
            estadoVehiculo = em.merge(estadoVehiculo);
            if (idTipoEstadoVehiculoOld != null && !idTipoEstadoVehiculoOld.equals(idTipoEstadoVehiculoNew)) {
                idTipoEstadoVehiculoOld.getEstadoVehiculoList().remove(estadoVehiculo);
                idTipoEstadoVehiculoOld = em.merge(idTipoEstadoVehiculoOld);
            }
            if (idTipoEstadoVehiculoNew != null && !idTipoEstadoVehiculoNew.equals(idTipoEstadoVehiculoOld)) {
                idTipoEstadoVehiculoNew.getEstadoVehiculoList().add(estadoVehiculo);
                idTipoEstadoVehiculoNew = em.merge(idTipoEstadoVehiculoNew);
            }
            if (idVehiculoOld != null && !idVehiculoOld.equals(idVehiculoNew)) {
                idVehiculoOld.getEstadoVehiculoList().remove(estadoVehiculo);
                idVehiculoOld = em.merge(idVehiculoOld);
            }
            if (idVehiculoNew != null && !idVehiculoNew.equals(idVehiculoOld)) {
                idVehiculoNew.getEstadoVehiculoList().add(estadoVehiculo);
                idVehiculoNew = em.merge(idVehiculoNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = estadoVehiculo.getIdEstadoVehiculo();
                if (findEstadoVehiculo(id) == null) {
                    throw new NonexistentEntityException("The estadoVehiculo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            EstadoVehiculo estadoVehiculo;
            try {
                estadoVehiculo = em.getReference(EstadoVehiculo.class, id);
                estadoVehiculo.getIdEstadoVehiculo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The estadoVehiculo with id " + id + " no longer exists.", enfe);
            }
            TipoEstadoVehiculo idTipoEstadoVehiculo = estadoVehiculo.getIdTipoEstadoVehiculo();
            if (idTipoEstadoVehiculo != null) {
                idTipoEstadoVehiculo.getEstadoVehiculoList().remove(estadoVehiculo);
                idTipoEstadoVehiculo = em.merge(idTipoEstadoVehiculo);
            }
            Vehiculo idVehiculo = estadoVehiculo.getIdVehiculo();
            if (idVehiculo != null) {
                idVehiculo.getEstadoVehiculoList().remove(estadoVehiculo);
                idVehiculo = em.merge(idVehiculo);
            }
            em.remove(estadoVehiculo);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<EstadoVehiculo> findEstadoVehiculoEntities() {
        return findEstadoVehiculoEntities(true, -1, -1);
    }

    public List<EstadoVehiculo> findEstadoVehiculoEntities(int maxResults, int firstResult) {
        return findEstadoVehiculoEntities(false, maxResults, firstResult);
    }

    private List<EstadoVehiculo> findEstadoVehiculoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(EstadoVehiculo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public EstadoVehiculo findEstadoVehiculo(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(EstadoVehiculo.class, id);
        } finally {
            em.close();
        }
    }

    public int getEstadoVehiculoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<EstadoVehiculo> rt = cq.from(EstadoVehiculo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
