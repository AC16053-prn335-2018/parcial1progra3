/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import uesocc.edu.sv.ingenieria.prn335.entidades.Viaje;
import uesocc.edu.sv.ingenieria.prn335.entidades.Vehiculo;
import uesocc.edu.sv.ingenieria.prn335.entidades.TipoUsuario;
import uesocc.edu.sv.ingenieria.prn335.entidades.EstadoReserva;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.Reserva;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.IllegalOrphanException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class ReservaJpaController implements Serializable {

    public ReservaJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Reserva reserva) throws RollbackFailureException, Exception {
        if (reserva.getEstadoReservaList() == null) {
            reserva.setEstadoReservaList(new ArrayList<EstadoReserva>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Viaje viaje = reserva.getViaje();
            if (viaje != null) {
                viaje = em.getReference(viaje.getClass(), viaje.getIdReserva());
                reserva.setViaje(viaje);
            }
            Vehiculo idVehiculo = reserva.getIdVehiculo();
            if (idVehiculo != null) {
                idVehiculo = em.getReference(idVehiculo.getClass(), idVehiculo.getIdVehiculo());
                reserva.setIdVehiculo(idVehiculo);
            }
            TipoUsuario idTipoUsuario = reserva.getIdTipoUsuario();
            if (idTipoUsuario != null) {
                idTipoUsuario = em.getReference(idTipoUsuario.getClass(), idTipoUsuario.getIdTipoUsuario());
                reserva.setIdTipoUsuario(idTipoUsuario);
            }
            List<EstadoReserva> attachedEstadoReservaList = new ArrayList<EstadoReserva>();
            for (EstadoReserva estadoReservaListEstadoReservaToAttach : reserva.getEstadoReservaList()) {
                estadoReservaListEstadoReservaToAttach = em.getReference(estadoReservaListEstadoReservaToAttach.getClass(), estadoReservaListEstadoReservaToAttach.getIdEstadoReserva());
                attachedEstadoReservaList.add(estadoReservaListEstadoReservaToAttach);
            }
            reserva.setEstadoReservaList(attachedEstadoReservaList);
            em.persist(reserva);
            if (viaje != null) {
                Reserva oldReservaOfViaje = viaje.getReserva();
                if (oldReservaOfViaje != null) {
                    oldReservaOfViaje.setViaje(null);
                    oldReservaOfViaje = em.merge(oldReservaOfViaje);
                }
                viaje.setReserva(reserva);
                viaje = em.merge(viaje);
            }
            if (idVehiculo != null) {
                idVehiculo.getReservaList().add(reserva);
                idVehiculo = em.merge(idVehiculo);
            }
            if (idTipoUsuario != null) {
                idTipoUsuario.getReservaList().add(reserva);
                idTipoUsuario = em.merge(idTipoUsuario);
            }
            for (EstadoReserva estadoReservaListEstadoReserva : reserva.getEstadoReservaList()) {
                Reserva oldIdReservaOfEstadoReservaListEstadoReserva = estadoReservaListEstadoReserva.getIdReserva();
                estadoReservaListEstadoReserva.setIdReserva(reserva);
                estadoReservaListEstadoReserva = em.merge(estadoReservaListEstadoReserva);
                if (oldIdReservaOfEstadoReservaListEstadoReserva != null) {
                    oldIdReservaOfEstadoReservaListEstadoReserva.getEstadoReservaList().remove(estadoReservaListEstadoReserva);
                    oldIdReservaOfEstadoReservaListEstadoReserva = em.merge(oldIdReservaOfEstadoReservaListEstadoReserva);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Reserva reserva) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Reserva persistentReserva = em.find(Reserva.class, reserva.getIdReserva());
            Viaje viajeOld = persistentReserva.getViaje();
            Viaje viajeNew = reserva.getViaje();
            Vehiculo idVehiculoOld = persistentReserva.getIdVehiculo();
            Vehiculo idVehiculoNew = reserva.getIdVehiculo();
            TipoUsuario idTipoUsuarioOld = persistentReserva.getIdTipoUsuario();
            TipoUsuario idTipoUsuarioNew = reserva.getIdTipoUsuario();
            List<EstadoReserva> estadoReservaListOld = persistentReserva.getEstadoReservaList();
            List<EstadoReserva> estadoReservaListNew = reserva.getEstadoReservaList();
            List<String> illegalOrphanMessages = null;
            if (viajeOld != null && !viajeOld.equals(viajeNew)) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("You must retain Viaje " + viajeOld + " since its reserva field is not nullable.");
            }
            for (EstadoReserva estadoReservaListOldEstadoReserva : estadoReservaListOld) {
                if (!estadoReservaListNew.contains(estadoReservaListOldEstadoReserva)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain EstadoReserva " + estadoReservaListOldEstadoReserva + " since its idReserva field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (viajeNew != null) {
                viajeNew = em.getReference(viajeNew.getClass(), viajeNew.getIdReserva());
                reserva.setViaje(viajeNew);
            }
            if (idVehiculoNew != null) {
                idVehiculoNew = em.getReference(idVehiculoNew.getClass(), idVehiculoNew.getIdVehiculo());
                reserva.setIdVehiculo(idVehiculoNew);
            }
            if (idTipoUsuarioNew != null) {
                idTipoUsuarioNew = em.getReference(idTipoUsuarioNew.getClass(), idTipoUsuarioNew.getIdTipoUsuario());
                reserva.setIdTipoUsuario(idTipoUsuarioNew);
            }
            List<EstadoReserva> attachedEstadoReservaListNew = new ArrayList<EstadoReserva>();
            for (EstadoReserva estadoReservaListNewEstadoReservaToAttach : estadoReservaListNew) {
                estadoReservaListNewEstadoReservaToAttach = em.getReference(estadoReservaListNewEstadoReservaToAttach.getClass(), estadoReservaListNewEstadoReservaToAttach.getIdEstadoReserva());
                attachedEstadoReservaListNew.add(estadoReservaListNewEstadoReservaToAttach);
            }
            estadoReservaListNew = attachedEstadoReservaListNew;
            reserva.setEstadoReservaList(estadoReservaListNew);
            reserva = em.merge(reserva);
            if (viajeNew != null && !viajeNew.equals(viajeOld)) {
                Reserva oldReservaOfViaje = viajeNew.getReserva();
                if (oldReservaOfViaje != null) {
                    oldReservaOfViaje.setViaje(null);
                    oldReservaOfViaje = em.merge(oldReservaOfViaje);
                }
                viajeNew.setReserva(reserva);
                viajeNew = em.merge(viajeNew);
            }
            if (idVehiculoOld != null && !idVehiculoOld.equals(idVehiculoNew)) {
                idVehiculoOld.getReservaList().remove(reserva);
                idVehiculoOld = em.merge(idVehiculoOld);
            }
            if (idVehiculoNew != null && !idVehiculoNew.equals(idVehiculoOld)) {
                idVehiculoNew.getReservaList().add(reserva);
                idVehiculoNew = em.merge(idVehiculoNew);
            }
            if (idTipoUsuarioOld != null && !idTipoUsuarioOld.equals(idTipoUsuarioNew)) {
                idTipoUsuarioOld.getReservaList().remove(reserva);
                idTipoUsuarioOld = em.merge(idTipoUsuarioOld);
            }
            if (idTipoUsuarioNew != null && !idTipoUsuarioNew.equals(idTipoUsuarioOld)) {
                idTipoUsuarioNew.getReservaList().add(reserva);
                idTipoUsuarioNew = em.merge(idTipoUsuarioNew);
            }
            for (EstadoReserva estadoReservaListNewEstadoReserva : estadoReservaListNew) {
                if (!estadoReservaListOld.contains(estadoReservaListNewEstadoReserva)) {
                    Reserva oldIdReservaOfEstadoReservaListNewEstadoReserva = estadoReservaListNewEstadoReserva.getIdReserva();
                    estadoReservaListNewEstadoReserva.setIdReserva(reserva);
                    estadoReservaListNewEstadoReserva = em.merge(estadoReservaListNewEstadoReserva);
                    if (oldIdReservaOfEstadoReservaListNewEstadoReserva != null && !oldIdReservaOfEstadoReservaListNewEstadoReserva.equals(reserva)) {
                        oldIdReservaOfEstadoReservaListNewEstadoReserva.getEstadoReservaList().remove(estadoReservaListNewEstadoReserva);
                        oldIdReservaOfEstadoReservaListNewEstadoReserva = em.merge(oldIdReservaOfEstadoReservaListNewEstadoReserva);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = reserva.getIdReserva();
                if (findReserva(id) == null) {
                    throw new NonexistentEntityException("The reserva with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Reserva reserva;
            try {
                reserva = em.getReference(Reserva.class, id);
                reserva.getIdReserva();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The reserva with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Viaje viajeOrphanCheck = reserva.getViaje();
            if (viajeOrphanCheck != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Reserva (" + reserva + ") cannot be destroyed since the Viaje " + viajeOrphanCheck + " in its viaje field has a non-nullable reserva field.");
            }
            List<EstadoReserva> estadoReservaListOrphanCheck = reserva.getEstadoReservaList();
            for (EstadoReserva estadoReservaListOrphanCheckEstadoReserva : estadoReservaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Reserva (" + reserva + ") cannot be destroyed since the EstadoReserva " + estadoReservaListOrphanCheckEstadoReserva + " in its estadoReservaList field has a non-nullable idReserva field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Vehiculo idVehiculo = reserva.getIdVehiculo();
            if (idVehiculo != null) {
                idVehiculo.getReservaList().remove(reserva);
                idVehiculo = em.merge(idVehiculo);
            }
            TipoUsuario idTipoUsuario = reserva.getIdTipoUsuario();
            if (idTipoUsuario != null) {
                idTipoUsuario.getReservaList().remove(reserva);
                idTipoUsuario = em.merge(idTipoUsuario);
            }
            em.remove(reserva);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Reserva> findReservaEntities() {
        return findReservaEntities(true, -1, -1);
    }

    public List<Reserva> findReservaEntities(int maxResults, int firstResult) {
        return findReservaEntities(false, maxResults, firstResult);
    }

    private List<Reserva> findReservaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Reserva.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Reserva findReserva(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Reserva.class, id);
        } finally {
            em.close();
        }
    }

    public int getReservaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Reserva> rt = cq.from(Reserva.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
