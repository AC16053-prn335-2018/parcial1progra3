/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.Recorrido;
import uesocc.edu.sv.ingenieria.prn335.entidades.Viaje;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class RecorridoJpaController implements Serializable {

    public RecorridoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Recorrido recorrido) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Viaje idReserva = recorrido.getIdReserva();
            if (idReserva != null) {
                idReserva = em.getReference(idReserva.getClass(), idReserva.getIdReserva());
                recorrido.setIdReserva(idReserva);
            }
            em.persist(recorrido);
            if (idReserva != null) {
                idReserva.getRecorridoList().add(recorrido);
                idReserva = em.merge(idReserva);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Recorrido recorrido) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Recorrido persistentRecorrido = em.find(Recorrido.class, recorrido.getIdRecorrido());
            Viaje idReservaOld = persistentRecorrido.getIdReserva();
            Viaje idReservaNew = recorrido.getIdReserva();
            if (idReservaNew != null) {
                idReservaNew = em.getReference(idReservaNew.getClass(), idReservaNew.getIdReserva());
                recorrido.setIdReserva(idReservaNew);
            }
            recorrido = em.merge(recorrido);
            if (idReservaOld != null && !idReservaOld.equals(idReservaNew)) {
                idReservaOld.getRecorridoList().remove(recorrido);
                idReservaOld = em.merge(idReservaOld);
            }
            if (idReservaNew != null && !idReservaNew.equals(idReservaOld)) {
                idReservaNew.getRecorridoList().add(recorrido);
                idReservaNew = em.merge(idReservaNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = recorrido.getIdRecorrido();
                if (findRecorrido(id) == null) {
                    throw new NonexistentEntityException("The recorrido with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Recorrido recorrido;
            try {
                recorrido = em.getReference(Recorrido.class, id);
                recorrido.getIdRecorrido();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The recorrido with id " + id + " no longer exists.", enfe);
            }
            Viaje idReserva = recorrido.getIdReserva();
            if (idReserva != null) {
                idReserva.getRecorridoList().remove(recorrido);
                idReserva = em.merge(idReserva);
            }
            em.remove(recorrido);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Recorrido> findRecorridoEntities() {
        return findRecorridoEntities(true, -1, -1);
    }

    public List<Recorrido> findRecorridoEntities(int maxResults, int firstResult) {
        return findRecorridoEntities(false, maxResults, firstResult);
    }

    private List<Recorrido> findRecorridoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Recorrido.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Recorrido findRecorrido(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Recorrido.class, id);
        } finally {
            em.close();
        }
    }

    public int getRecorridoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Recorrido> rt = cq.from(Recorrido.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
