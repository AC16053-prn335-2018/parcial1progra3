/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceUnit;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.EstadoReserva;
import uesocc.edu.sv.ingenieria.prn335.entidades.TipoEstadoReserva;
import uesocc.edu.sv.ingenieria.prn335.entidades.Reserva;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class EstadoReservaJpaController implements Serializable {

    public EstadoReservaJpaController() {
    }
    @Resource //inject from your application server
UserTransaction utx; 
   @PersistenceUnit(unitName="uesocc.edu.sv.ingenieria_Flotilla_war_1.0-SNAPSHOTPU") //inject from your application server
EntityManagerFactory emf;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(EstadoReserva estadoReserva) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoEstadoReserva idTipoEstadoReserva = estadoReserva.getIdTipoEstadoReserva();
            if (idTipoEstadoReserva != null) {
                idTipoEstadoReserva = em.getReference(idTipoEstadoReserva.getClass(), idTipoEstadoReserva.getIdTipoEstadoReserva());
                estadoReserva.setIdTipoEstadoReserva(idTipoEstadoReserva);
            }
            Reserva idReserva = estadoReserva.getIdReserva();
            if (idReserva != null) {
                idReserva = em.getReference(idReserva.getClass(), idReserva.getIdReserva());
                estadoReserva.setIdReserva(idReserva);
            }
            em.persist(estadoReserva);
            if (idTipoEstadoReserva != null) {
                idTipoEstadoReserva.getEstadoReservaList().add(estadoReserva);
                idTipoEstadoReserva = em.merge(idTipoEstadoReserva);
            }
            if (idReserva != null) {
                idReserva.getEstadoReservaList().add(estadoReserva);
                idReserva = em.merge(idReserva);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(EstadoReserva estadoReserva) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            EstadoReserva persistentEstadoReserva = em.find(EstadoReserva.class, estadoReserva.getIdEstadoReserva());
            TipoEstadoReserva idTipoEstadoReservaOld = persistentEstadoReserva.getIdTipoEstadoReserva();
            TipoEstadoReserva idTipoEstadoReservaNew = estadoReserva.getIdTipoEstadoReserva();
            Reserva idReservaOld = persistentEstadoReserva.getIdReserva();
            Reserva idReservaNew = estadoReserva.getIdReserva();
            if (idTipoEstadoReservaNew != null) {
                idTipoEstadoReservaNew = em.getReference(idTipoEstadoReservaNew.getClass(), idTipoEstadoReservaNew.getIdTipoEstadoReserva());
                estadoReserva.setIdTipoEstadoReserva(idTipoEstadoReservaNew);
            }
            if (idReservaNew != null) {
                idReservaNew = em.getReference(idReservaNew.getClass(), idReservaNew.getIdReserva());
                estadoReserva.setIdReserva(idReservaNew);
            }
            estadoReserva = em.merge(estadoReserva);
            if (idTipoEstadoReservaOld != null && !idTipoEstadoReservaOld.equals(idTipoEstadoReservaNew)) {
                idTipoEstadoReservaOld.getEstadoReservaList().remove(estadoReserva);
                idTipoEstadoReservaOld = em.merge(idTipoEstadoReservaOld);
            }
            if (idTipoEstadoReservaNew != null && !idTipoEstadoReservaNew.equals(idTipoEstadoReservaOld)) {
                idTipoEstadoReservaNew.getEstadoReservaList().add(estadoReserva);
                idTipoEstadoReservaNew = em.merge(idTipoEstadoReservaNew);
            }
            if (idReservaOld != null && !idReservaOld.equals(idReservaNew)) {
                idReservaOld.getEstadoReservaList().remove(estadoReserva);
                idReservaOld = em.merge(idReservaOld);
            }
            if (idReservaNew != null && !idReservaNew.equals(idReservaOld)) {
                idReservaNew.getEstadoReservaList().add(estadoReserva);
                idReservaNew = em.merge(idReservaNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = estadoReserva.getIdEstadoReserva();
                if (findEstadoReserva(id) == null) {
                    throw new NonexistentEntityException("The estadoReserva with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            EstadoReserva estadoReserva;
            try {
                estadoReserva = em.getReference(EstadoReserva.class, id);
                estadoReserva.getIdEstadoReserva();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The estadoReserva with id " + id + " no longer exists.", enfe);
            }
            TipoEstadoReserva idTipoEstadoReserva = estadoReserva.getIdTipoEstadoReserva();
            if (idTipoEstadoReserva != null) {
                idTipoEstadoReserva.getEstadoReservaList().remove(estadoReserva);
                idTipoEstadoReserva = em.merge(idTipoEstadoReserva);
            }
            Reserva idReserva = estadoReserva.getIdReserva();
            if (idReserva != null) {
                idReserva.getEstadoReservaList().remove(estadoReserva);
                idReserva = em.merge(idReserva);
            }
            em.remove(estadoReserva);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<EstadoReserva> findEstadoReservaEntities() {
        return findEstadoReservaEntities(true, -1, -1);
    }

    public List<EstadoReserva> findEstadoReservaEntities(int maxResults, int firstResult) {
        return findEstadoReservaEntities(false, maxResults, firstResult);
    }

    private List<EstadoReserva> findEstadoReservaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(EstadoReserva.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public EstadoReserva findEstadoReserva(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(EstadoReserva.class, id);
        } finally {
            em.close();
        }
    }

    public int getEstadoReservaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<EstadoReserva> rt = cq.from(EstadoReserva.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
