/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import uesocc.edu.sv.ingenieria.prn335.entidades.EstadoReserva;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.TipoEstadoReserva;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.IllegalOrphanException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class TipoEstadoReservaJpaController implements Serializable {

    public TipoEstadoReservaJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoEstadoReserva tipoEstadoReserva) throws RollbackFailureException, Exception {
        if (tipoEstadoReserva.getEstadoReservaList() == null) {
            tipoEstadoReserva.setEstadoReservaList(new ArrayList<EstadoReserva>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<EstadoReserva> attachedEstadoReservaList = new ArrayList<EstadoReserva>();
            for (EstadoReserva estadoReservaListEstadoReservaToAttach : tipoEstadoReserva.getEstadoReservaList()) {
                estadoReservaListEstadoReservaToAttach = em.getReference(estadoReservaListEstadoReservaToAttach.getClass(), estadoReservaListEstadoReservaToAttach.getIdEstadoReserva());
                attachedEstadoReservaList.add(estadoReservaListEstadoReservaToAttach);
            }
            tipoEstadoReserva.setEstadoReservaList(attachedEstadoReservaList);
            em.persist(tipoEstadoReserva);
            for (EstadoReserva estadoReservaListEstadoReserva : tipoEstadoReserva.getEstadoReservaList()) {
                TipoEstadoReserva oldIdTipoEstadoReservaOfEstadoReservaListEstadoReserva = estadoReservaListEstadoReserva.getIdTipoEstadoReserva();
                estadoReservaListEstadoReserva.setIdTipoEstadoReserva(tipoEstadoReserva);
                estadoReservaListEstadoReserva = em.merge(estadoReservaListEstadoReserva);
                if (oldIdTipoEstadoReservaOfEstadoReservaListEstadoReserva != null) {
                    oldIdTipoEstadoReservaOfEstadoReservaListEstadoReserva.getEstadoReservaList().remove(estadoReservaListEstadoReserva);
                    oldIdTipoEstadoReservaOfEstadoReservaListEstadoReserva = em.merge(oldIdTipoEstadoReservaOfEstadoReservaListEstadoReserva);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoEstadoReserva tipoEstadoReserva) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoEstadoReserva persistentTipoEstadoReserva = em.find(TipoEstadoReserva.class, tipoEstadoReserva.getIdTipoEstadoReserva());
            List<EstadoReserva> estadoReservaListOld = persistentTipoEstadoReserva.getEstadoReservaList();
            List<EstadoReserva> estadoReservaListNew = tipoEstadoReserva.getEstadoReservaList();
            List<String> illegalOrphanMessages = null;
            for (EstadoReserva estadoReservaListOldEstadoReserva : estadoReservaListOld) {
                if (!estadoReservaListNew.contains(estadoReservaListOldEstadoReserva)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain EstadoReserva " + estadoReservaListOldEstadoReserva + " since its idTipoEstadoReserva field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<EstadoReserva> attachedEstadoReservaListNew = new ArrayList<EstadoReserva>();
            for (EstadoReserva estadoReservaListNewEstadoReservaToAttach : estadoReservaListNew) {
                estadoReservaListNewEstadoReservaToAttach = em.getReference(estadoReservaListNewEstadoReservaToAttach.getClass(), estadoReservaListNewEstadoReservaToAttach.getIdEstadoReserva());
                attachedEstadoReservaListNew.add(estadoReservaListNewEstadoReservaToAttach);
            }
            estadoReservaListNew = attachedEstadoReservaListNew;
            tipoEstadoReserva.setEstadoReservaList(estadoReservaListNew);
            tipoEstadoReserva = em.merge(tipoEstadoReserva);
            for (EstadoReserva estadoReservaListNewEstadoReserva : estadoReservaListNew) {
                if (!estadoReservaListOld.contains(estadoReservaListNewEstadoReserva)) {
                    TipoEstadoReserva oldIdTipoEstadoReservaOfEstadoReservaListNewEstadoReserva = estadoReservaListNewEstadoReserva.getIdTipoEstadoReserva();
                    estadoReservaListNewEstadoReserva.setIdTipoEstadoReserva(tipoEstadoReserva);
                    estadoReservaListNewEstadoReserva = em.merge(estadoReservaListNewEstadoReserva);
                    if (oldIdTipoEstadoReservaOfEstadoReservaListNewEstadoReserva != null && !oldIdTipoEstadoReservaOfEstadoReservaListNewEstadoReserva.equals(tipoEstadoReserva)) {
                        oldIdTipoEstadoReservaOfEstadoReservaListNewEstadoReserva.getEstadoReservaList().remove(estadoReservaListNewEstadoReserva);
                        oldIdTipoEstadoReservaOfEstadoReservaListNewEstadoReserva = em.merge(oldIdTipoEstadoReservaOfEstadoReservaListNewEstadoReserva);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipoEstadoReserva.getIdTipoEstadoReserva();
                if (findTipoEstadoReserva(id) == null) {
                    throw new NonexistentEntityException("The tipoEstadoReserva with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoEstadoReserva tipoEstadoReserva;
            try {
                tipoEstadoReserva = em.getReference(TipoEstadoReserva.class, id);
                tipoEstadoReserva.getIdTipoEstadoReserva();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoEstadoReserva with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<EstadoReserva> estadoReservaListOrphanCheck = tipoEstadoReserva.getEstadoReservaList();
            for (EstadoReserva estadoReservaListOrphanCheckEstadoReserva : estadoReservaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TipoEstadoReserva (" + tipoEstadoReserva + ") cannot be destroyed since the EstadoReserva " + estadoReservaListOrphanCheckEstadoReserva + " in its estadoReservaList field has a non-nullable idTipoEstadoReserva field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipoEstadoReserva);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoEstadoReserva> findTipoEstadoReservaEntities() {
        return findTipoEstadoReservaEntities(true, -1, -1);
    }

    public List<TipoEstadoReserva> findTipoEstadoReservaEntities(int maxResults, int firstResult) {
        return findTipoEstadoReservaEntities(false, maxResults, firstResult);
    }

    private List<TipoEstadoReserva> findTipoEstadoReservaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoEstadoReserva.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoEstadoReserva findTipoEstadoReserva(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoEstadoReserva.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoEstadoReservaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoEstadoReserva> rt = cq.from(TipoEstadoReserva.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
