/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import uesocc.edu.sv.ingenieria.prn335.entidades.Marca;
import uesocc.edu.sv.ingenieria.prn335.entidades.TipoVehiculo;
import uesocc.edu.sv.ingenieria.prn335.entidades.Vehiculo;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.Modelo;
import uesocc.edu.sv.ingenieria.prn335.entidades.ModeloParte;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.IllegalOrphanException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class ModeloJpaController implements Serializable {

    public ModeloJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Modelo modelo) throws RollbackFailureException, Exception {
        if (modelo.getVehiculoList() == null) {
            modelo.setVehiculoList(new ArrayList<Vehiculo>());
        }
        if (modelo.getModeloParteList() == null) {
            modelo.setModeloParteList(new ArrayList<ModeloParte>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Marca idMarca = modelo.getIdMarca();
            if (idMarca != null) {
                idMarca = em.getReference(idMarca.getClass(), idMarca.getIdMarca());
                modelo.setIdMarca(idMarca);
            }
            TipoVehiculo idTipoVehiculo = modelo.getIdTipoVehiculo();
            if (idTipoVehiculo != null) {
                idTipoVehiculo = em.getReference(idTipoVehiculo.getClass(), idTipoVehiculo.getIdTipoVehiculo());
                modelo.setIdTipoVehiculo(idTipoVehiculo);
            }
            List<Vehiculo> attachedVehiculoList = new ArrayList<Vehiculo>();
            for (Vehiculo vehiculoListVehiculoToAttach : modelo.getVehiculoList()) {
                vehiculoListVehiculoToAttach = em.getReference(vehiculoListVehiculoToAttach.getClass(), vehiculoListVehiculoToAttach.getIdVehiculo());
                attachedVehiculoList.add(vehiculoListVehiculoToAttach);
            }
            modelo.setVehiculoList(attachedVehiculoList);
            List<ModeloParte> attachedModeloParteList = new ArrayList<ModeloParte>();
            for (ModeloParte modeloParteListModeloParteToAttach : modelo.getModeloParteList()) {
                modeloParteListModeloParteToAttach = em.getReference(modeloParteListModeloParteToAttach.getClass(), modeloParteListModeloParteToAttach.getIdModeloParte());
                attachedModeloParteList.add(modeloParteListModeloParteToAttach);
            }
            modelo.setModeloParteList(attachedModeloParteList);
            em.persist(modelo);
            if (idMarca != null) {
                idMarca.getModeloList().add(modelo);
                idMarca = em.merge(idMarca);
            }
            if (idTipoVehiculo != null) {
                idTipoVehiculo.getModeloList().add(modelo);
                idTipoVehiculo = em.merge(idTipoVehiculo);
            }
            for (Vehiculo vehiculoListVehiculo : modelo.getVehiculoList()) {
                Modelo oldIdModeloOfVehiculoListVehiculo = vehiculoListVehiculo.getIdModelo();
                vehiculoListVehiculo.setIdModelo(modelo);
                vehiculoListVehiculo = em.merge(vehiculoListVehiculo);
                if (oldIdModeloOfVehiculoListVehiculo != null) {
                    oldIdModeloOfVehiculoListVehiculo.getVehiculoList().remove(vehiculoListVehiculo);
                    oldIdModeloOfVehiculoListVehiculo = em.merge(oldIdModeloOfVehiculoListVehiculo);
                }
            }
            for (ModeloParte modeloParteListModeloParte : modelo.getModeloParteList()) {
                Modelo oldIdModeloOfModeloParteListModeloParte = modeloParteListModeloParte.getIdModelo();
                modeloParteListModeloParte.setIdModelo(modelo);
                modeloParteListModeloParte = em.merge(modeloParteListModeloParte);
                if (oldIdModeloOfModeloParteListModeloParte != null) {
                    oldIdModeloOfModeloParteListModeloParte.getModeloParteList().remove(modeloParteListModeloParte);
                    oldIdModeloOfModeloParteListModeloParte = em.merge(oldIdModeloOfModeloParteListModeloParte);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Modelo modelo) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Modelo persistentModelo = em.find(Modelo.class, modelo.getIdModelo());
            Marca idMarcaOld = persistentModelo.getIdMarca();
            Marca idMarcaNew = modelo.getIdMarca();
            TipoVehiculo idTipoVehiculoOld = persistentModelo.getIdTipoVehiculo();
            TipoVehiculo idTipoVehiculoNew = modelo.getIdTipoVehiculo();
            List<Vehiculo> vehiculoListOld = persistentModelo.getVehiculoList();
            List<Vehiculo> vehiculoListNew = modelo.getVehiculoList();
            List<ModeloParte> modeloParteListOld = persistentModelo.getModeloParteList();
            List<ModeloParte> modeloParteListNew = modelo.getModeloParteList();
            List<String> illegalOrphanMessages = null;
            for (Vehiculo vehiculoListOldVehiculo : vehiculoListOld) {
                if (!vehiculoListNew.contains(vehiculoListOldVehiculo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Vehiculo " + vehiculoListOldVehiculo + " since its idModelo field is not nullable.");
                }
            }
            for (ModeloParte modeloParteListOldModeloParte : modeloParteListOld) {
                if (!modeloParteListNew.contains(modeloParteListOldModeloParte)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ModeloParte " + modeloParteListOldModeloParte + " since its idModelo field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idMarcaNew != null) {
                idMarcaNew = em.getReference(idMarcaNew.getClass(), idMarcaNew.getIdMarca());
                modelo.setIdMarca(idMarcaNew);
            }
            if (idTipoVehiculoNew != null) {
                idTipoVehiculoNew = em.getReference(idTipoVehiculoNew.getClass(), idTipoVehiculoNew.getIdTipoVehiculo());
                modelo.setIdTipoVehiculo(idTipoVehiculoNew);
            }
            List<Vehiculo> attachedVehiculoListNew = new ArrayList<Vehiculo>();
            for (Vehiculo vehiculoListNewVehiculoToAttach : vehiculoListNew) {
                vehiculoListNewVehiculoToAttach = em.getReference(vehiculoListNewVehiculoToAttach.getClass(), vehiculoListNewVehiculoToAttach.getIdVehiculo());
                attachedVehiculoListNew.add(vehiculoListNewVehiculoToAttach);
            }
            vehiculoListNew = attachedVehiculoListNew;
            modelo.setVehiculoList(vehiculoListNew);
            List<ModeloParte> attachedModeloParteListNew = new ArrayList<ModeloParte>();
            for (ModeloParte modeloParteListNewModeloParteToAttach : modeloParteListNew) {
                modeloParteListNewModeloParteToAttach = em.getReference(modeloParteListNewModeloParteToAttach.getClass(), modeloParteListNewModeloParteToAttach.getIdModeloParte());
                attachedModeloParteListNew.add(modeloParteListNewModeloParteToAttach);
            }
            modeloParteListNew = attachedModeloParteListNew;
            modelo.setModeloParteList(modeloParteListNew);
            modelo = em.merge(modelo);
            if (idMarcaOld != null && !idMarcaOld.equals(idMarcaNew)) {
                idMarcaOld.getModeloList().remove(modelo);
                idMarcaOld = em.merge(idMarcaOld);
            }
            if (idMarcaNew != null && !idMarcaNew.equals(idMarcaOld)) {
                idMarcaNew.getModeloList().add(modelo);
                idMarcaNew = em.merge(idMarcaNew);
            }
            if (idTipoVehiculoOld != null && !idTipoVehiculoOld.equals(idTipoVehiculoNew)) {
                idTipoVehiculoOld.getModeloList().remove(modelo);
                idTipoVehiculoOld = em.merge(idTipoVehiculoOld);
            }
            if (idTipoVehiculoNew != null && !idTipoVehiculoNew.equals(idTipoVehiculoOld)) {
                idTipoVehiculoNew.getModeloList().add(modelo);
                idTipoVehiculoNew = em.merge(idTipoVehiculoNew);
            }
            for (Vehiculo vehiculoListNewVehiculo : vehiculoListNew) {
                if (!vehiculoListOld.contains(vehiculoListNewVehiculo)) {
                    Modelo oldIdModeloOfVehiculoListNewVehiculo = vehiculoListNewVehiculo.getIdModelo();
                    vehiculoListNewVehiculo.setIdModelo(modelo);
                    vehiculoListNewVehiculo = em.merge(vehiculoListNewVehiculo);
                    if (oldIdModeloOfVehiculoListNewVehiculo != null && !oldIdModeloOfVehiculoListNewVehiculo.equals(modelo)) {
                        oldIdModeloOfVehiculoListNewVehiculo.getVehiculoList().remove(vehiculoListNewVehiculo);
                        oldIdModeloOfVehiculoListNewVehiculo = em.merge(oldIdModeloOfVehiculoListNewVehiculo);
                    }
                }
            }
            for (ModeloParte modeloParteListNewModeloParte : modeloParteListNew) {
                if (!modeloParteListOld.contains(modeloParteListNewModeloParte)) {
                    Modelo oldIdModeloOfModeloParteListNewModeloParte = modeloParteListNewModeloParte.getIdModelo();
                    modeloParteListNewModeloParte.setIdModelo(modelo);
                    modeloParteListNewModeloParte = em.merge(modeloParteListNewModeloParte);
                    if (oldIdModeloOfModeloParteListNewModeloParte != null && !oldIdModeloOfModeloParteListNewModeloParte.equals(modelo)) {
                        oldIdModeloOfModeloParteListNewModeloParte.getModeloParteList().remove(modeloParteListNewModeloParte);
                        oldIdModeloOfModeloParteListNewModeloParte = em.merge(oldIdModeloOfModeloParteListNewModeloParte);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = modelo.getIdModelo();
                if (findModelo(id) == null) {
                    throw new NonexistentEntityException("The modelo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Modelo modelo;
            try {
                modelo = em.getReference(Modelo.class, id);
                modelo.getIdModelo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The modelo with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Vehiculo> vehiculoListOrphanCheck = modelo.getVehiculoList();
            for (Vehiculo vehiculoListOrphanCheckVehiculo : vehiculoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Modelo (" + modelo + ") cannot be destroyed since the Vehiculo " + vehiculoListOrphanCheckVehiculo + " in its vehiculoList field has a non-nullable idModelo field.");
            }
            List<ModeloParte> modeloParteListOrphanCheck = modelo.getModeloParteList();
            for (ModeloParte modeloParteListOrphanCheckModeloParte : modeloParteListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Modelo (" + modelo + ") cannot be destroyed since the ModeloParte " + modeloParteListOrphanCheckModeloParte + " in its modeloParteList field has a non-nullable idModelo field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Marca idMarca = modelo.getIdMarca();
            if (idMarca != null) {
                idMarca.getModeloList().remove(modelo);
                idMarca = em.merge(idMarca);
            }
            TipoVehiculo idTipoVehiculo = modelo.getIdTipoVehiculo();
            if (idTipoVehiculo != null) {
                idTipoVehiculo.getModeloList().remove(modelo);
                idTipoVehiculo = em.merge(idTipoVehiculo);
            }
            em.remove(modelo);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Modelo> findModeloEntities() {
        return findModeloEntities(true, -1, -1);
    }

    public List<Modelo> findModeloEntities(int maxResults, int firstResult) {
        return findModeloEntities(false, maxResults, firstResult);
    }

    private List<Modelo> findModeloEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Modelo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Modelo findModelo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Modelo.class, id);
        } finally {
            em.close();
        }
    }

    public int getModeloCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Modelo> rt = cq.from(Modelo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
