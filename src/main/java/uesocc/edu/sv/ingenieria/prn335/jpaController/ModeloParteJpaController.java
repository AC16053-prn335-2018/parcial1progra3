/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.Modelo;
import uesocc.edu.sv.ingenieria.prn335.entidades.ModeloParte;
import uesocc.edu.sv.ingenieria.prn335.entidades.Parte;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class ModeloParteJpaController implements Serializable {

    public ModeloParteJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ModeloParte modeloParte) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Modelo idModelo = modeloParte.getIdModelo();
            if (idModelo != null) {
                idModelo = em.getReference(idModelo.getClass(), idModelo.getIdModelo());
                modeloParte.setIdModelo(idModelo);
            }
            Parte idParte = modeloParte.getIdParte();
            if (idParte != null) {
                idParte = em.getReference(idParte.getClass(), idParte.getIdParte());
                modeloParte.setIdParte(idParte);
            }
            em.persist(modeloParte);
            if (idModelo != null) {
                idModelo.getModeloParteList().add(modeloParte);
                idModelo = em.merge(idModelo);
            }
            if (idParte != null) {
                idParte.getModeloParteList().add(modeloParte);
                idParte = em.merge(idParte);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ModeloParte modeloParte) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            ModeloParte persistentModeloParte = em.find(ModeloParte.class, modeloParte.getIdModeloParte());
            Modelo idModeloOld = persistentModeloParte.getIdModelo();
            Modelo idModeloNew = modeloParte.getIdModelo();
            Parte idParteOld = persistentModeloParte.getIdParte();
            Parte idParteNew = modeloParte.getIdParte();
            if (idModeloNew != null) {
                idModeloNew = em.getReference(idModeloNew.getClass(), idModeloNew.getIdModelo());
                modeloParte.setIdModelo(idModeloNew);
            }
            if (idParteNew != null) {
                idParteNew = em.getReference(idParteNew.getClass(), idParteNew.getIdParte());
                modeloParte.setIdParte(idParteNew);
            }
            modeloParte = em.merge(modeloParte);
            if (idModeloOld != null && !idModeloOld.equals(idModeloNew)) {
                idModeloOld.getModeloParteList().remove(modeloParte);
                idModeloOld = em.merge(idModeloOld);
            }
            if (idModeloNew != null && !idModeloNew.equals(idModeloOld)) {
                idModeloNew.getModeloParteList().add(modeloParte);
                idModeloNew = em.merge(idModeloNew);
            }
            if (idParteOld != null && !idParteOld.equals(idParteNew)) {
                idParteOld.getModeloParteList().remove(modeloParte);
                idParteOld = em.merge(idParteOld);
            }
            if (idParteNew != null && !idParteNew.equals(idParteOld)) {
                idParteNew.getModeloParteList().add(modeloParte);
                idParteNew = em.merge(idParteNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = modeloParte.getIdModeloParte();
                if (findModeloParte(id) == null) {
                    throw new NonexistentEntityException("The modeloParte with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            ModeloParte modeloParte;
            try {
                modeloParte = em.getReference(ModeloParte.class, id);
                modeloParte.getIdModeloParte();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The modeloParte with id " + id + " no longer exists.", enfe);
            }
            Modelo idModelo = modeloParte.getIdModelo();
            if (idModelo != null) {
                idModelo.getModeloParteList().remove(modeloParte);
                idModelo = em.merge(idModelo);
            }
            Parte idParte = modeloParte.getIdParte();
            if (idParte != null) {
                idParte.getModeloParteList().remove(modeloParte);
                idParte = em.merge(idParte);
            }
            em.remove(modeloParte);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ModeloParte> findModeloParteEntities() {
        return findModeloParteEntities(true, -1, -1);
    }

    public List<ModeloParte> findModeloParteEntities(int maxResults, int firstResult) {
        return findModeloParteEntities(false, maxResults, firstResult);
    }

    private List<ModeloParte> findModeloParteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ModeloParte.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ModeloParte findModeloParte(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ModeloParte.class, id);
        } finally {
            em.close();
        }
    }

    public int getModeloParteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ModeloParte> rt = cq.from(ModeloParte.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
