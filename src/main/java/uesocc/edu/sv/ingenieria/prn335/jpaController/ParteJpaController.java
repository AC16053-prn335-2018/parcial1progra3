/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import uesocc.edu.sv.ingenieria.prn335.entidades.SubTipoParte;
import uesocc.edu.sv.ingenieria.prn335.entidades.ModeloParte;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.Parte;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.IllegalOrphanException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class ParteJpaController implements Serializable {

    public ParteJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Parte parte) throws RollbackFailureException, Exception {
        if (parte.getModeloParteList() == null) {
            parte.setModeloParteList(new ArrayList<ModeloParte>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            SubTipoParte idSubTipoParte = parte.getIdSubTipoParte();
            if (idSubTipoParte != null) {
                idSubTipoParte = em.getReference(idSubTipoParte.getClass(), idSubTipoParte.getIdSubTipoParte());
                parte.setIdSubTipoParte(idSubTipoParte);
            }
            List<ModeloParte> attachedModeloParteList = new ArrayList<ModeloParte>();
            for (ModeloParte modeloParteListModeloParteToAttach : parte.getModeloParteList()) {
                modeloParteListModeloParteToAttach = em.getReference(modeloParteListModeloParteToAttach.getClass(), modeloParteListModeloParteToAttach.getIdModeloParte());
                attachedModeloParteList.add(modeloParteListModeloParteToAttach);
            }
            parte.setModeloParteList(attachedModeloParteList);
            em.persist(parte);
            if (idSubTipoParte != null) {
                idSubTipoParte.getParteList().add(parte);
                idSubTipoParte = em.merge(idSubTipoParte);
            }
            for (ModeloParte modeloParteListModeloParte : parte.getModeloParteList()) {
                Parte oldIdParteOfModeloParteListModeloParte = modeloParteListModeloParte.getIdParte();
                modeloParteListModeloParte.setIdParte(parte);
                modeloParteListModeloParte = em.merge(modeloParteListModeloParte);
                if (oldIdParteOfModeloParteListModeloParte != null) {
                    oldIdParteOfModeloParteListModeloParte.getModeloParteList().remove(modeloParteListModeloParte);
                    oldIdParteOfModeloParteListModeloParte = em.merge(oldIdParteOfModeloParteListModeloParte);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Parte parte) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Parte persistentParte = em.find(Parte.class, parte.getIdParte());
            SubTipoParte idSubTipoParteOld = persistentParte.getIdSubTipoParte();
            SubTipoParte idSubTipoParteNew = parte.getIdSubTipoParte();
            List<ModeloParte> modeloParteListOld = persistentParte.getModeloParteList();
            List<ModeloParte> modeloParteListNew = parte.getModeloParteList();
            List<String> illegalOrphanMessages = null;
            for (ModeloParte modeloParteListOldModeloParte : modeloParteListOld) {
                if (!modeloParteListNew.contains(modeloParteListOldModeloParte)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ModeloParte " + modeloParteListOldModeloParte + " since its idParte field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idSubTipoParteNew != null) {
                idSubTipoParteNew = em.getReference(idSubTipoParteNew.getClass(), idSubTipoParteNew.getIdSubTipoParte());
                parte.setIdSubTipoParte(idSubTipoParteNew);
            }
            List<ModeloParte> attachedModeloParteListNew = new ArrayList<ModeloParte>();
            for (ModeloParte modeloParteListNewModeloParteToAttach : modeloParteListNew) {
                modeloParteListNewModeloParteToAttach = em.getReference(modeloParteListNewModeloParteToAttach.getClass(), modeloParteListNewModeloParteToAttach.getIdModeloParte());
                attachedModeloParteListNew.add(modeloParteListNewModeloParteToAttach);
            }
            modeloParteListNew = attachedModeloParteListNew;
            parte.setModeloParteList(modeloParteListNew);
            parte = em.merge(parte);
            if (idSubTipoParteOld != null && !idSubTipoParteOld.equals(idSubTipoParteNew)) {
                idSubTipoParteOld.getParteList().remove(parte);
                idSubTipoParteOld = em.merge(idSubTipoParteOld);
            }
            if (idSubTipoParteNew != null && !idSubTipoParteNew.equals(idSubTipoParteOld)) {
                idSubTipoParteNew.getParteList().add(parte);
                idSubTipoParteNew = em.merge(idSubTipoParteNew);
            }
            for (ModeloParte modeloParteListNewModeloParte : modeloParteListNew) {
                if (!modeloParteListOld.contains(modeloParteListNewModeloParte)) {
                    Parte oldIdParteOfModeloParteListNewModeloParte = modeloParteListNewModeloParte.getIdParte();
                    modeloParteListNewModeloParte.setIdParte(parte);
                    modeloParteListNewModeloParte = em.merge(modeloParteListNewModeloParte);
                    if (oldIdParteOfModeloParteListNewModeloParte != null && !oldIdParteOfModeloParteListNewModeloParte.equals(parte)) {
                        oldIdParteOfModeloParteListNewModeloParte.getModeloParteList().remove(modeloParteListNewModeloParte);
                        oldIdParteOfModeloParteListNewModeloParte = em.merge(oldIdParteOfModeloParteListNewModeloParte);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = parte.getIdParte();
                if (findParte(id) == null) {
                    throw new NonexistentEntityException("The parte with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Parte parte;
            try {
                parte = em.getReference(Parte.class, id);
                parte.getIdParte();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The parte with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<ModeloParte> modeloParteListOrphanCheck = parte.getModeloParteList();
            for (ModeloParte modeloParteListOrphanCheckModeloParte : modeloParteListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Parte (" + parte + ") cannot be destroyed since the ModeloParte " + modeloParteListOrphanCheckModeloParte + " in its modeloParteList field has a non-nullable idParte field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            SubTipoParte idSubTipoParte = parte.getIdSubTipoParte();
            if (idSubTipoParte != null) {
                idSubTipoParte.getParteList().remove(parte);
                idSubTipoParte = em.merge(idSubTipoParte);
            }
            em.remove(parte);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Parte> findParteEntities() {
        return findParteEntities(true, -1, -1);
    }

    public List<Parte> findParteEntities(int maxResults, int firstResult) {
        return findParteEntities(false, maxResults, firstResult);
    }

    private List<Parte> findParteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Parte.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Parte findParte(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Parte.class, id);
        } finally {
            em.close();
        }
    }

    public int getParteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Parte> rt = cq.from(Parte.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
