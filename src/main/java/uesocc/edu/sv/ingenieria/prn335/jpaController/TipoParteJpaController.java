/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.jpaController;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import uesocc.edu.sv.ingenieria.prn335.entidades.SubTipoParte;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import uesocc.edu.sv.ingenieria.prn335.entidades.TipoParte;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.IllegalOrphanException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.NonexistentEntityException;
import uesocc.edu.sv.ingenieria.prn335.jpaController.exceptions.RollbackFailureException;

/**
 *
 * @author cristian
 */
public class TipoParteJpaController implements Serializable {

    public TipoParteJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoParte tipoParte) throws RollbackFailureException, Exception {
        if (tipoParte.getSubTipoParteList() == null) {
            tipoParte.setSubTipoParteList(new ArrayList<SubTipoParte>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<SubTipoParte> attachedSubTipoParteList = new ArrayList<SubTipoParte>();
            for (SubTipoParte subTipoParteListSubTipoParteToAttach : tipoParte.getSubTipoParteList()) {
                subTipoParteListSubTipoParteToAttach = em.getReference(subTipoParteListSubTipoParteToAttach.getClass(), subTipoParteListSubTipoParteToAttach.getIdSubTipoParte());
                attachedSubTipoParteList.add(subTipoParteListSubTipoParteToAttach);
            }
            tipoParte.setSubTipoParteList(attachedSubTipoParteList);
            em.persist(tipoParte);
            for (SubTipoParte subTipoParteListSubTipoParte : tipoParte.getSubTipoParteList()) {
                TipoParte oldIdTipoParteOfSubTipoParteListSubTipoParte = subTipoParteListSubTipoParte.getIdTipoParte();
                subTipoParteListSubTipoParte.setIdTipoParte(tipoParte);
                subTipoParteListSubTipoParte = em.merge(subTipoParteListSubTipoParte);
                if (oldIdTipoParteOfSubTipoParteListSubTipoParte != null) {
                    oldIdTipoParteOfSubTipoParteListSubTipoParte.getSubTipoParteList().remove(subTipoParteListSubTipoParte);
                    oldIdTipoParteOfSubTipoParteListSubTipoParte = em.merge(oldIdTipoParteOfSubTipoParteListSubTipoParte);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoParte tipoParte) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoParte persistentTipoParte = em.find(TipoParte.class, tipoParte.getIdTipoParte());
            List<SubTipoParte> subTipoParteListOld = persistentTipoParte.getSubTipoParteList();
            List<SubTipoParte> subTipoParteListNew = tipoParte.getSubTipoParteList();
            List<String> illegalOrphanMessages = null;
            for (SubTipoParte subTipoParteListOldSubTipoParte : subTipoParteListOld) {
                if (!subTipoParteListNew.contains(subTipoParteListOldSubTipoParte)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SubTipoParte " + subTipoParteListOldSubTipoParte + " since its idTipoParte field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<SubTipoParte> attachedSubTipoParteListNew = new ArrayList<SubTipoParte>();
            for (SubTipoParte subTipoParteListNewSubTipoParteToAttach : subTipoParteListNew) {
                subTipoParteListNewSubTipoParteToAttach = em.getReference(subTipoParteListNewSubTipoParteToAttach.getClass(), subTipoParteListNewSubTipoParteToAttach.getIdSubTipoParte());
                attachedSubTipoParteListNew.add(subTipoParteListNewSubTipoParteToAttach);
            }
            subTipoParteListNew = attachedSubTipoParteListNew;
            tipoParte.setSubTipoParteList(subTipoParteListNew);
            tipoParte = em.merge(tipoParte);
            for (SubTipoParte subTipoParteListNewSubTipoParte : subTipoParteListNew) {
                if (!subTipoParteListOld.contains(subTipoParteListNewSubTipoParte)) {
                    TipoParte oldIdTipoParteOfSubTipoParteListNewSubTipoParte = subTipoParteListNewSubTipoParte.getIdTipoParte();
                    subTipoParteListNewSubTipoParte.setIdTipoParte(tipoParte);
                    subTipoParteListNewSubTipoParte = em.merge(subTipoParteListNewSubTipoParte);
                    if (oldIdTipoParteOfSubTipoParteListNewSubTipoParte != null && !oldIdTipoParteOfSubTipoParteListNewSubTipoParte.equals(tipoParte)) {
                        oldIdTipoParteOfSubTipoParteListNewSubTipoParte.getSubTipoParteList().remove(subTipoParteListNewSubTipoParte);
                        oldIdTipoParteOfSubTipoParteListNewSubTipoParte = em.merge(oldIdTipoParteOfSubTipoParteListNewSubTipoParte);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipoParte.getIdTipoParte();
                if (findTipoParte(id) == null) {
                    throw new NonexistentEntityException("The tipoParte with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoParte tipoParte;
            try {
                tipoParte = em.getReference(TipoParte.class, id);
                tipoParte.getIdTipoParte();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoParte with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<SubTipoParte> subTipoParteListOrphanCheck = tipoParte.getSubTipoParteList();
            for (SubTipoParte subTipoParteListOrphanCheckSubTipoParte : subTipoParteListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TipoParte (" + tipoParte + ") cannot be destroyed since the SubTipoParte " + subTipoParteListOrphanCheckSubTipoParte + " in its subTipoParteList field has a non-nullable idTipoParte field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipoParte);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoParte> findTipoParteEntities() {
        return findTipoParteEntities(true, -1, -1);
    }

    public List<TipoParte> findTipoParteEntities(int maxResults, int firstResult) {
        return findTipoParteEntities(false, maxResults, firstResult);
    }

    private List<TipoParte> findTipoParteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoParte.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoParte findTipoParte(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoParte.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoParteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoParte> rt = cq.from(TipoParte.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
