package main;

import uesocc.edu.sv.ingenieria.prn335.entidades.Marca;
import uesocc.edu.sv.ingenieria.prn335.jpaController.MarcaJpaController;

public class NewMain {

    public static void main(String[] args) {
        Marca marca = new Marca();
        MarcaJpaController marcaControlador = new MarcaJpaController();
        marca.setActivo(true);
        marca.setDescripcion("Hola como está?");
        marca.setNombre("Kawasaky");
        try {
            System.out.println("estoy adentro del try");
            marcaControlador.create(marca);
            System.out.println("Sigo acá!");
        } catch (Exception ex) {
            System.out.println("No manches dio Error:" + ex.getMessage());
        }

    }

}
